%define oraepoch 2
%define phpdir /usr/lib64/php/modules
%define phpapi 20170718-64
%define oravers 19.3


Summary: PHP Extension for Oracle Database
Name: php-oci8
Version: 2.2.0
Release: 1%{?dist}
URL: http://pecl.php.net/package/oci8/
Source0: http://pecl.php.net/get/oci8-%{version}.tgz
License: PHP
Group: Development/Languages
Requires: %{phpdir}
Requires: php(api) = %{phpapi}

BuildRoot: %{_tmppath}/%{name}-root
BuildRequires: php-devel
Buildrequires: oracle-instantclient%{oravers}-devel

# no (recent) instantclient for ia64

# adapt to the way we build the instantclient at CERN, and drop the
# "--rpath" to find different instantclient libs. Needs CFLAGS/LDFLAGS
# below.
Patch1: php-oci8-lib64.patch

ExclusiveArch: x86_64

%if 0%{?rhel} == 8
Requires: oracle-instantclient%{oravers}-basic
Requires: cern-oracle-instantclient-basic >= %{oraepoch}:%{oravers}
Requires: cern-oracle-instantclient%{oravers}-meta
%else%
Requires: oracle-instantclient%{oravers}-basic
Requires: oracle-instantclient-basic >= %{oraepoch}:%{oravers}
Requires: oracle-instantclient%{oravers}-meta
%endif%

%description
This PHP extension allows you to access Oracle databases using the
Oracle Call Interface (OCI8).

It has been linked against the ORACLE instantclient %{oravers} but
should work with others as long as the libraries are found in your
search path.
It should work with any PHP that claims Api version %{phpapi}.

%prep
%setup -q -n oci8-%{version}
#patch1 -p1

%build
phpize
export OCI8_INCLUDES=/usr/include/oracle/%{oravers}/client64/
export OCI8_LIB_DIR=/usr/lib/oracle/%{oravers}/client64/lib/

export CPPFLAGS=-I/usr/include/oracle/%{oravers}/client64/
export LDFLAGS=-L/usr/lib/oracle/%{oravers}/client64/%{_lib}
./configure --with-oci8=shared,/usr/lib/oracle/%{oravers}/client64/
make

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%phpdir/
install -m 755 modules/oci8.so $RPM_BUILD_ROOT%phpdir/

mkdir -p $RPM_BUILD_ROOT/etc/php.d/
cat > $RPM_BUILD_ROOT/etc/php.d/oci8.ini <<EOF
; Enable oci8 extension module
extension=oci8.so
EOF



%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README CREDITS tests
%config /etc/php.d/oci8.ini 
%phpdir/oci8.so

%changelog
* Wed Mar 04 2020 Ben Morrice <ben.morrice@cern.ch> - 2.2.0-1
- use v2.2.0 for php7 support

* Wed Oct 23 2019 Ben Morrice <ben.morrice@cern.ch> - 2.0.8-5
- rebuild for 19.3

* Fri Sep 15 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 2.0.8-4
- rebuild for 12.2

* Thu Dec 03 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.0.8-3
- fix reqs again.

* Fri Sep 12 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.0.8-2
- fix reqs

* Wed Sep 10 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.0.8
- el7 initial build

* Fri Nov 30 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> -1.4.9-1
- unified build for 5/6, update to latest.

* Fri Nov 18 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> -1.4.6-1.slc6
- updated to 1.4.6
* Tue Dec  7 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> -1.4.4-1.slc6
- new build on slc6
* Tue Feb  3 2009 Jan Iven <jan.iven@cern.ch> - 1.3.4-1.slc5
- generic build - no need to rebuild on PHP/instantclient updates, as long as the ABI is fixed..


