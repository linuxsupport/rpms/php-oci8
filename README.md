# php-oci8

* As php7 is not available in CC7, this repository currently builds v2.2.0
  for C8 only. rpmci no longer builds for EL7
* The last release for CC7 (php 5.4) is v2.0.8
